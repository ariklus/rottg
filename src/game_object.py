# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 22:21:44 2017

@author: ariklus
"""
import sys
from time import gmtime, strftime

from src.config.color_config import popup_color, bg_color
from src.config.events_config import *
from src.config.font_config import buttonfont_loc, regular_text_loc
from src.config.general_config import default_fps
from src.config.path_config import save_path
from src.utils import Button, TransferButton


class GameObject(object):
    bg_color = bg_color
    frame_rate = default_fps
    screen_name = None
    dirty_rects = []
    buttons = []
    events = []
    player = {}
    flags = {}
    screen_data = None

    def __init__(self, gamedata):
        self.session_data = gamedata
        if 'player' in self.session_data.keys():
            self.player = self.session_data['player']
            self.flags = self.session_data['flags']
        else:
            self.session_data['player'] = self.player
            self.session_data['flags'] = self.flags

        if 'options' in self.session_data.keys():
            self.options = self.session_data['options']
        else:
            self.load_opfions()
            self.options = self.session_data['options']
            self.apply_options()

        self.clock = pygame.time.Clock()
        self.screen = pygame.display.get_surface()
        self.button_font = pygame.font.Font(buttonfont_loc, 28)
        self.big_button_font = pygame.font.Font(buttonfont_loc, 54)
        self.regular_text_font = pygame.font.Font(regular_text_loc, 28)
        self.dirty_rects = [self.screen.get_rect()]
        self.clean_buttons()

    @staticmethod
    def text_objects(text, font, color):
        text_surface = font.render(text, True, color)
        return text_surface, text_surface.get_rect()

    def load_popup(self):
        popup_size = [300, 150]
        popup_rect = [(self.session_data['size'][0] - popup_size[0]) / 2,
                      (self.session_data['size'][1] - popup_size[1]) / 2,
                      popup_size]
        pygame.draw.rect(
            self.screen, popup_color, popup_rect)
        pygame.display.update(self.dirty_rects)

    def save(self, save_number):
        save = {'player_stats': self.session_data['player'],
                'story_flags': self.session_data['flags'],
                'time': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                'screen': self.screen_name,
                'screen_data': self.screen_data
                }
        f = open('save/' + str(save_number) + '.sav', 'a')
        f.seek(0)
        f.truncate()
        f.write(str(save))

    def load_save(self, save_number):
        data = eval(open(save_path + str(save_number) + '.sav', 'r').read())
        self.player = data['player_stats']
        self.flags = data['story_flags']
        self.session_data['player'] = self.player
        self.session_data['flags'] = self.flags
        self.session_data['save_slot'] = save_number
        return [data['screen'], data['screen_data']]

    def new_game(self, slot_number):
        ng_data = eval(open('src/ng.sav', 'r').read())
        ng_data['time'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        f = open('save/' + str(slot_number) + '.sav', 'a')
        f.seek(0)
        f.truncate()
        f.write(str(ng_data))
        f.close()
        return self.load_save(slot_number)

    def load_opfions(self):
        data = eval(open('src/config/options').read())
        self.session_data['options'] = data

    def save_options(self):
        save = self.options
        f = open('src/config/options', 'a')
        f.seek(0)
        f.truncate()
        f.write(str(save))

    # Apply options on start of game
    def apply_options(self):
        if self.options['fullscreen']:
            event = pygame.event.Event(MENUEVENT, data='Fullscreen')
            pygame.event.post(event)
        for flag in self.options['flags']:
            self.flags[flag] = self.options['flags'][flag]['on']

    @staticmethod
    def aspect_scale(processed_image, size: tuple):
        """ Scales 'img' to fit into box bx/by.
         This method will retain the original image's aspect ratio """
        bx = size[0]
        by = size[1]
        ix, iy = processed_image.get_size()
        if ix > iy:
            # fit to width
            scale_factor = bx / float(ix)
            sy = scale_factor * iy
            if sy > by:
                scale_factor = by / float(iy)
                sx = scale_factor * ix
                sy = by
            else:
                sx = bx
        else:
            # fit to height
            scale_factor = by / float(iy)
            sx = scale_factor * ix
            if sx > bx:
                scale_factor = bx / float(ix)
                sx = bx
                sy = scale_factor * iy
            else:
                sy = by

        return pygame.transform.scale(processed_image, (int(sx), int(sy)))

    def clear(self):
        self.clean_buttons()

    def go_fullscreen(self):
        self.options['fullscreen'] = True
        pygame.display.quit()
        pygame.display.init()
        pygame.display.set_mode(self.session_data['size'], pygame.FULLSCREEN)
        self.screen = pygame.display.get_surface()
        self.run_frame()
        self.dirty_rects.append(self.screen.get_rect())

    def go_windowed(self):
        self.options['fullscreen'] = False
        pygame.display.quit()
        pygame.display.init()
        pygame.display.set_mode(self.session_data['size'])
        self.screen = pygame.display.get_surface()
        self.run_frame()
        self.dirty_rects.append(self.screen.get_rect())

    def blit_text(self, position, text, font, color, bgcolor=None, bg_extend=0,
                  pos_type='center', alpha=None):
        text_surface = font.render(text, True, color)
        text_rect = pygame.Rect(text_surface.get_rect())
        if alpha:
            text_surface.set_alpha(alpha)
        if pos_type == 'center':
            text_rect.center = position
        elif pos_type == 'topleft':
            text_rect.topleft = position
        if bgcolor is None:
            self.screen.blit(text_surface, text_rect)
        else:
            if bg_extend > 0:
                text_rect = text_rect.inflate(bg_extend * 2, bg_extend * 2)
            bgsurf = pygame.Surface((text_rect.width, text_rect.height))
            bgsurf.set_alpha(alpha)
            bgsurf.fill(bgcolor)
            self.screen.blit(bgsurf, text_rect.topleft)
            self.screen.blit(text_surface, text_rect)

        return text_rect

    def add_button(self, rect, text, font, inactive_color, active_color, text_color, action):
        self.buttons.append(Button(pygame.Rect(rect), text, font,
                                   inactive_color, active_color, text_color, action))
        self.dirty_rects.append(rect)

    def add_transfer_button(self, rect, text, font, inactive_color, active_color, text_color, action):
        self.buttons.append(TransferButton(pygame.Rect(rect), text, font,
                                           inactive_color, active_color, text_color, action))
        self.dirty_rects.append(rect)

    def clean_buttons(self):
        del self.buttons[:]

    def blit_buttons(self, buttons):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        for button in buttons:

            blit = False
            if button.rect.collidepoint(mouse):
                color = button.active_color
                if button.active is True:

                    if click[0] == 1:
                        button.pressed = True
                    elif click[0] == 0 and button.pressed:
                        pygame.event.post(button.event)

                else:
                    blit = True
                    button.active = True
            else:
                button.pressed = False
                color = button.inactive_color
                if button.active:
                    button.active = False
                    blit = True

            pygame.draw.rect(
                self.screen, color, button.rect)
            text_surf, text_rect = self.text_objects(
                button.text, button.font, button.text_color)
            text_rect.center = button.rect.center
            self.screen.blit(text_surf, text_rect)
            if blit is True:
                self.dirty_rects.append(button.rect)

    def apply_changes(self, changes):

        if 'stat_change' in changes.keys():
            for change in changes['stat_change'].keys():
                if change not in self.player.keys():
                    self.player[change] = 0
                self.player[change] += changes['stat_change'][change]

        if 'flag_change' in changes.keys():
            for change in changes['flag_change'].keys():
                self.flags[change] = changes['flag_change'][change]

        print(self.player)
        print(self.flags)

    def process_events(self):
        self.events = pygame.event.get()
        for event in self.events:
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    sys.exit()
                if event.key == pygame.K_f:
                    self.go_fullscreen()
                if event.key == pygame.K_w:
                    self.go_windowed()
            if event.type is MENUEVENT:
                if event.data == 'Exit':
                    sys.exit()
                elif event.data == 'Quit':
                    return ['mmenu']
                elif event.data == 'Options':
                    return ['options']
                elif event.data == 'Fullscreen':
                    self.go_fullscreen()
                elif event.data == 'Save':
                    self.save(self.session_data['save_slot'])
            if event.type is TRANSFEREVENT:
                return [event.data['screen'], event.data['content']]

    def check_condition(self, condition_type, condition_name, comparation):
        if condition_type == 'stat':
            if condition_name in self.player.keys():
                return eval(str(self.player[condition_name]) + comparation)
            else:
                return eval('0' + comparation)
        elif condition_type == 'flag':
            if condition_name in self.flags.keys():
                return self.flags[condition_name] is comparation
            else:
                return False is comparation
        else:
            raise NameError("invalid stat type")

    def run_frame(self):
        next_screen = self.process_events()
        if next_screen is not None:
            del self.dirty_rects[:]
            return next_screen
        self.blit_buttons(self.buttons)

        self.clock.tick(self.frame_rate)
        pygame.display.update(self.dirty_rects)
        self.dirty_rects = []

        return [self.screen_name]
