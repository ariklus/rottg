import os

import pygame

from src.config.color_config import inactive_button_color, active_button_color, black
from src.config.events_config import MENUEVENT
from src.config.path_config import save_path
from src.game_object import GameObject


class LoadScreen(GameObject):
    screen_name = 'load'

    def __init__(self, game_data, mode='load'):
        super(LoadScreen, self).__init__(game_data)
        self.mode = mode

        self.button_rect = pygame.Rect(self.screen.get_width() * 0.3,
                                       self.screen.get_height() * 0.3,
                                       self.screen.get_width() * 0.4,
                                       self.screen.get_height() * 0.1)

        for i in xrange(1, 6):
            file_path = save_path + str(i) + '.sav'
            if os.path.isfile(file_path):
                time = eval(open(file_path).read())['time']
                button_text = str(i) + ': ' + time
                action = {'action': mode, 'slot': str(i)}
            else:
                button_text = str(i) + ': Empty'
                if mode == 'new':
                    action = {'action': mode, 'slot': str(i)}
                else:
                    action = None

            self.add_button(self.button_rect, button_text, self.regular_text_font, inactive_button_color,
                            active_button_color, black, action)
            self.button_rect.top += self.screen.get_height() * 0.12

    def process_events(self):
        tmp = super(LoadScreen, self).process_events()
        if tmp:
            return tmp

        for event in self.events:
            if event.type == MENUEVENT:
                if type(event.data) == dict:
                    if event.data['action'] == 'load':
                        return self.load_save(event.data['slot'])
                    elif event.data['action'] == 'new':
                        return self.new_game(event.data['slot'])
