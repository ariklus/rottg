import copy

import pygame
from src.config.color_config import inactive_button_color, black, bg_color, active_button_color
from src.config.events_config import RADIOEVENT, MENUEVENT
from src.config.font_config import title_font_loc, regular_text_loc
from src.game_object import GameObject
from src.utils import SwitchButton


class Options(GameObject):
    screen_name = 'options'
    button_size = [200, 50]

    switches = []

    def __init__(self, game_data):
        super(Options, self).__init__(game_data)
        self.temp_options = copy.deepcopy(self.options)
        self.title_center = self.screen.get_width() * 0.5, self.screen.get_height() * 0.1
        self.title_font = pygame.font.Font(title_font_loc, 50)
        self.regular_font = pygame.font.Font(regular_text_loc, 28)

        fs_center = self.screen.get_width() * 0.25, self.screen.get_height() * 0.2
        fs_rect = pygame.Rect(0, 0, self.screen.get_width() * 0.25, self.screen.get_height() * 0.1)
        fs_rect.center = fs_center
        self.switches.append(
            SwitchButton(fs_rect, 'Windowed', 'Fullscreen', self.regular_font, black, 'go_windowed', 'go_fullscreen',
                         inactive_button_color, active_button_color, self.options['fullscreen'])
        )

        fs_rect.top += self.screen.get_height() * 0.2
        self.switches.append(
            SwitchButton(fs_rect, 'OFF', 'ON', self.regular_font, black, 'autosave_off', 'autosave_on',
                         inactive_button_color, active_button_color, self.options['autosave'], header='Autosave')
        )

        fs_center = self.screen.get_width() * 0.75, self.screen.get_height() * 0.2
        fs_rect = pygame.Rect(0, 0, self.screen.get_width() * 0.25, self.screen.get_height() * 0.06)
        fs_rect.center = fs_center
        for key in self.options['flags'].keys():

            self.switches.append(
                SwitchButton(fs_rect, 'Off', 'On', self.regular_font, black, {'flag': key, 'action': False},
                             {'flag': key, 'action': True}, inactive_button_color, active_button_color,
                             self.options['flags'][key]['on'], header=self.options['flags'][key]['alias'])
            )

            fs_rect.top += self.screen.get_height() * 0.15

        button_rect = pygame.Rect(0, 0, self.button_size[0], self.button_size[1])
        button_rect.center = (self.screen.get_width() * 0.25, self.screen.get_height() * 0.9)
        self.add_button(button_rect, 'Save', self.button_font,
                        inactive_button_color, active_button_color, black, 'Save')
        button_rect.left += self.screen.get_width() * 0.5
        self.add_button(button_rect, 'Cancel', self.button_font,
                        inactive_button_color, active_button_color, black, 'Cancel')

    def blit_header(self):
        self.blit_text(position=self.title_center,
                       text='Options',
                       font=self.title_font,
                       color=black)

    def blit_switches(self):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        for switch in self.switches:

            if switch.clickable_rect().collidepoint(mouse):
                if click[0] == 1 and switch.pressed is False:
                    switch.pressed = True
                elif click[0] == 0 and switch.pressed is True:
                    event = switch.switch()
                    pygame.event.post(event)

                    self.dirty_rects.append(switch.rect)
            elif switch.pressed is True:
                switch.pressed = False

            pygame.draw.rect(self.screen, switch.left_color(), switch.left_rect)
            text_surf, text_rect = self.text_objects(
                switch.left_text, switch.font, switch.text_color)
            text_rect.center = switch.left_rect.center
            self.screen.blit(text_surf, text_rect)

            pygame.draw.rect(self.screen, switch.right_color(), switch.right_rect)
            text_surf, text_rect = self.text_objects(
                switch.right_text, switch.font, switch.text_color)
            text_rect.center = switch.right_rect.center
            self.screen.blit(text_surf, text_rect)

            if switch.header is not None:
                text_center = switch.rect.center

                text_surf, text_rect = self.text_objects(
                    switch.header, self.regular_font, black)
                text_rect.center = (text_center[0], text_center[1] - switch.rect.height)
                self.screen.blit(text_surf, text_rect)
                self.dirty_rects.append(text_rect)

    def process_events(self):
        super_events = super(Options, self).process_events()
        if super_events:
            return super_events

        for event in self.events:
            if event.type == RADIOEVENT:
                if type(event.data) is dict:
                    self.temp_options['flags'][event.data['flag']]['on'] = event.data['action']
                elif event.data == 'go_fullscreen':
                    self.temp_options['fullscreen'] = True
                    self.go_fullscreen()
                elif event.data == 'go_windowed':
                    self.temp_options['fullscreen'] = False
                    self.go_windowed()
                elif event.data == 'autosave_off':
                    self.temp_options['autosave'] = False
                elif event.data == 'autosave_on':
                    self.temp_options['autosave'] = True
            elif event.type == MENUEVENT:
                if event.data == 'Save':
                    self.session_data['options'] = copy.deepcopy(self.temp_options)
                    self.save_options()
                    print(self.options)
                    self.apply_options()
                    return ['mmenu']
                elif event.data == 'Cancel':
                    return ['mmenu']

    def run_frame(self):
        self.screen.fill(bg_color)
        self.blit_header()
        self.blit_switches()

        return super(Options, self).run_frame()
