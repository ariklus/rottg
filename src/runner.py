# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 22:21:44 2017

@author: ariklus
"""
import sys
import pygame
from mmenu import MainMenu
from invade_mini import InvaderMini
from src.choice_popup import ChoiceMode
from src.load_screen import LoadScreen
from src.map_mode import MapMode
from src.options import Options
from story_mode import StoryMode


class Runner(object):

    def __init__(self, gamedata):
        self.game_data = gamedata

    def start_screen(self, nextscreen):
        screenname = nextscreen[0]

        if screenname == 'story':
            return StoryMode(self.game_data, nextscreen[1])
        elif screenname == 'invaders':
            return InvaderMini(self.game_data, nextscreen[1])
        elif screenname == 'choice':
            return ChoiceMode(self.game_data, nextscreen[1])
        elif screenname == 'map':
            return MapMode(self.game_data, nextscreen[1])
        elif screenname == 'load':
            return LoadScreen(self.game_data, nextscreen[1])
        elif screenname == 'mmenu':
            return MainMenu(self.game_data)
        elif screenname == 'options':
            return Options(self.game_data)
        elif screenname == 'exit':
            sys.exit()

    def run_game(self):

        pygame.init()
        icon = pygame.image.load('img/icon.png')
        pygame.display.set_icon(icon)
        pygame.display.set_mode(self.game_data['size'])
        pygame.display.set_caption(self.game_data['game_name'])

        active_screen_name = 'mmenu'
        active_screen = MainMenu(self.game_data)
        screen_next = ['mmenu']

        while True:
            while active_screen_name == screen_next[0]:
                screen_next = active_screen.run_frame()

            active_screen_name = screen_next[0]
            active_screen = self.start_screen(screen_next)
