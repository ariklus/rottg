import pygame
import random

# import random for random numbers!
import random

# import pygame.locals for easier access to key coordinates
from pygame.locals import *

def play():
    capital = long(2)
    roll = random.random()
    while roll > 0.5:
        capital *= 2
        roll = random.random()
    return capital

sum = 0
for i in xrange(1000000):
    sum += play()

sum /= 1000000

print sum

