import random

import pygame

from game_mode import GameMode
from src.config.color_config import inactive_button_color, black
from src.config.events_config import GAMEEVENT


class MiniGame(GameMode):
    pause = False

    def __init__(self, game_data, game_config):
        super(MiniGame, self).__init__(game_data)
        random.seed()
        self.win_effects = game_config['on_win']
        self.loss_effects = game_config['on_loss']

    def make_pause(self):
        if self.pause:
            self.pause = False
        else:
            self.pause = True
            pause_rect = pygame.Rect(0, 0,
                                     self.screen.get_width() * 0.3,
                                     self.screen.get_height() * 0.3)
            pause_rect.center = (self.screen.get_width() * 0.5, self.screen.get_height() * 0.5)
            pygame.draw.rect(self.screen, inactive_button_color, pause_rect)
            text_surf, text_rect = self.text_objects(
                'PAUSED', self.button_font, black)
            text_rect.center = pause_rect.center
            self.screen.blit(text_surf, text_rect)
            pygame.display.update(self.dirty_rects)
            while self.pause:
                self.process_events()

    def process_events(self):
        super_event = super(MiniGame, self).process_events()
        if super_event:
            return super_event
        for event in self.events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    self.make_pause()


    def win(self):
        self.apply_changes(self.win_effects)
        event = pygame.event.Event(
            GAMEEVENT, data=[self.win_effects["screen"], self.win_effects["content"]])
        pygame.event.post(event)

    def lose(self):
        self.apply_changes(self.loss_effects)
        event = pygame.event.Event(
            GAMEEVENT, data=[self.loss_effects["screen"], self.loss_effects["content"]])
        pygame.event.post(event)
