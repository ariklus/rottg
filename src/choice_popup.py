import pygame

from src.config.color_config import popup_color, black, inactive_button_color, active_button_color
from src.config.font_config import regular_text_loc
from src.game_mode import GameMode
from src.utils import ChoiceButton


class ChoiceMode(GameMode):
    screen_name = 'choice'
    menu_mode = 'story'
    choicebox_rel_size = [0.50, 0.80]
    choicebox_rel_position = [0.25, 0.1]

    def __init__(self, game_data, choice_details):
        super(ChoiceMode, self).__init__(game_data)
        self.choice_details = choice_details
        self.choice_rect = pygame.Rect(self.rel_gz_coord(self.choicebox_rel_position),
                                       self.rel_gz_coord(self.choicebox_rel_size))
        choice_button_size = self.rel_gz_coord([0.4, 0.1])

        self.text_font = pygame.font.Font(regular_text_loc, 28)

        pygame.draw.rect(self.screen, popup_color, self.choice_rect)
        self.blit_text(position=(self.choice_rect.centerx, self.choice_rect.top + 35),
                       text=choice_details["text"],
                       font=self.text_font,
                       color=black)

        button_rect = pygame.Rect([0, 0], choice_button_size)
        button_rect.center = [self.choice_rect.centerx,
                              self.choice_rect.top + self.choice_rect.height * 0.25]

        for button_info in choice_details["buttons"]:
            self.add_choice_button(button_rect, button_info["text"], self.text_font,
                                   inactive_button_color, active_button_color, black,
                                   button_info)
            button_rect.y += button_rect.height * 1.5

        self.dirty_rects.append(self.choice_rect)
        pygame.display.update(self.dirty_rects)

    def add_choice_button(self, rect, text, font, inactive_color, active_color, text_color, choice_effects):
        self.buttons.append(ChoiceButton(pygame.Rect(rect), text, font,
                                         inactive_color, active_color, text_color, choice_effects))
        self.dirty_rects.append(rect)

    def run_frame(self, menu_mode=None):
        return super(ChoiceMode, self).run_frame()
