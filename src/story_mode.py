import json
import random
import glob

import pygame
from game_mode import GameMode
from src.config.color_config import bg_color, black
from src.config.events_config import GAMEEVENT
from src.config.path_config import story_image_path, story_script_path


class StoryMode(GameMode):
    screen_name = 'story'
    menu_mode = 'story'
    story_img = None
    img_rect = None
    script = []
    current_text = ''
    text_visible = True
    placeholder = None
    position = 0

    def __init__(self, game_data, story_event):
        super(StoryMode, self).__init__(game_data)
        self.text_box_img = pygame.image.load(
            'img/text_box.png')
        self.text_box_rect = pygame.Rect([5, self.session_data['game_zone'][3] - 205,
                                          self.session_data['game_zone'][2] - 10, 200])
        self.text_rect = self.text_box_rect.inflate(-46, -20)

        if type(story_event) == dict:
            self.story_event = story_event['content']
            if 'start_line' in story_event.keys():
                start_position = int(story_event['start_line'])
            else:
                raise ValueError('Invalid_start_line_position')
        else:
            self.story_event = story_event
            start_position = 0
        self.load_script(self.story_event, start_position)
        self.next_line()

    def save(self, save_number):
        if self.position > 0:
            load_pos = self.position - 1
        else:
            load_pos = 0
        self.screen_data = {'content': self.story_event,
                            'start_line': load_pos}
        super(StoryMode, self).save(save_number)

    def run_frame(self, menu_mode='story'):
        self.screen.fill(bg_color)
        # Blit image, textbox, text
        if self.story_img is not None:
            self.screen.blit(self.story_img, self.img_rect)
        else:
            self.blit_image_placeholder()

        if self.text_visible:
            self.blit_current_text(self.current_text)

        return super(StoryMode, self).run_frame()

    def load_script(self, script_name, start_position=0):
        self.position = start_position
        filelist = glob.glob(story_script_path + '*' + script_name + '*')
        if len(filelist) > 0:
            if len(filelist) == 1:
                i = 0
            else:
                i = int(random.random() * (len(filelist)))
            script_path = filelist[i]
        else:
            raise NameError('No such script file: ' + str(script_name))
        json_data = open(script_path).read()
        self.script = json.loads(json_data)

        # if image is none, load last image blitted
        if start_position > 0 and self.story_img is None:
            img_search = start_position
            while img_search >= 0 and self.story_img is None:
                if self.script[img_search]['type'] == 'image':
                    self.process_script_line(self.script[img_search])
                    img_search = 0
                img_search -= 1

        # If there is no current text, last text is displayed
        if start_position > 0 and self.current_text is '':
            txt_search = start_position
            while txt_search >= 0 and self.current_text is '':
                if self.script[txt_search]['type'] == 'text':
                    self.process_script_line(self.script[txt_search])
                    txt_search = 0
                txt_search -= 1

    def process_events(self):
        tmp = super(StoryMode, self).process_events()
        if tmp:
            return tmp
        for event in self.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.session_data['game_zone'].collidepoint(pygame.mouse.get_pos()):
                    if event.button == 1:
                        if self.text_visible:
                            self.next_line()
                    elif event.button == 3:
                        self.switch_text_visibility()

    def switch_text_visibility(self):
        if self.text_visible:
            self.text_visible = False
        else:
            self.text_visible = True
        self.dirty_rects.append(self.text_box_rect)
        self.run_frame()

    def blit_current_text(self, lines):
        self.dirty_rects.append(self.text_box_rect)
        self.screen.blit(self.text_box_img, self.text_box_rect)
        maxlines = 5
        if len(lines) > maxlines:
            Exception('too many lines: won\'t fit in textbox')
        position = [self.text_rect.topleft[0], self.text_rect.topleft[1]]
        for line in lines:
            self.blit_text(position=position,
                           text=line,
                           font=self.regular_text_font,
                           color=black,
                           pos_type='topleft'
                           )
            position[1] += self.regular_text_font.get_height() * 1.4

    def load_img(self, imgname=None):
        if imgname is not None:
            self.dirty_rects.append(self.session_data['game_zone'])
            filelist = glob.glob(story_image_path + '*' + imgname + '*')
            if len(filelist) > 0:
                if len(filelist) == 1:
                    i = 0
                else:
                    i = int(random.random() * (len(filelist)))
                img_path = filelist[i]

                self.story_img = pygame.image.load(img_path)
                self.story_img = self.aspect_scale(self.story_img,
                                                   (self.session_data['game_zone'][2],
                                                    self.session_data['game_zone'][3]))
                self.img_rect = self.story_img.get_rect()
                self.img_rect.center = self.session_data['game_zone'].center

                self.screen.blit(self.story_img, self.img_rect)
                self.dirty_rects.append(self.img_rect)

            else:
                self.placeholder = imgname + " not found"
                self.screen.fill(bg_color)
                self.blit_image_placeholder()
                self.dirty_rects.append(self.session_data['game_zone'])
                self.story_img = None
        else:
            self.story_img = None

    def blit_image_placeholder(self):
        self.blit_text(self.rel_gz_coord([0.5, 0.05]), self.placeholder,
                       self.regular_text_font, black)

    def next_line(self):
        if self.position < len(self.script):
            self.process_script_line(self.script[self.position])
            self.position += 1
        else:
            print("Invalid script ending")

    def process_script_line(self, line):
        if 'stat_change' in line.keys() or 'flag_change' in line.keys():
            self.apply_changes(line)
        if 'widgets' in line.keys():
            for widgetline in line['widgets']:
                if widgetline['action'] == 'add':
                    self.widgets.append(widgetline['widget'])
                elif widgetline['action'] == 'clear':
                    del self.widgets[:]
            self.apply_changes()


        if line["type"] == 'text':
            self.current_text = line["content"]
            self.dirty_rects.append(self.text_box_rect)
        elif line["type"] == 'image':
            self.load_img(line["content"])
        elif line["type"] == 'condition':
            if self.check_condition(line['content']['condition']['type'],
                                    line['content']['condition']['name'],
                                    line['content']['condition']['comparation']):
                for outcome in line['content']['yeslines']:
                    self.process_script_line(outcome)
            else:
                for outcome in line['content']['nolines']:
                    self.process_script_line(outcome)

        # Processes outcomes based on chances then returns to further script
        elif line["type"] == 'random':
            roll = random.random()
            print( roll)
            print( line['chance'])
            if roll < float(line["chance"])/100:
                for outcome in line['content']['yeslines']:
                    self.process_script_line(outcome)
            else:
                for outcome in line['content']['nolines']:
                    self.process_script_line(outcome)
        elif line["type"] == 'transfer':
            if line["screen"] == 'story':
                self.story_event = line["content"]
                self.load_script(line["content"])
                self.process_script_line(self.script[0])
            else:
                event = pygame.event.Event(
                    GAMEEVENT, data=[line["screen"], line["content"]])
                pygame.event.post(event)
        else:
            print( "invalid type of line: " + line["type"] + " - skipping")
