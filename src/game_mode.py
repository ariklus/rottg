import glob
import random

import pygame

from game_object import GameObject
from src.config.color_config import bg_color, black, bar_bg_color
from src.config.font_config import regular_text_loc
from src.config.sidebar_configs import sidebars
from src.config.events_config import MENUEVENT, GAMEEVENT, CHOICEEVENT


class GameMode(GameObject):
    menu_buttons = []
    menu_mode = None
    sidebar_image_path = 'img/sidebar_img/'

    game_mode_buttons = {}
    widgets = []

    def __init__(self, game_data):
        super(GameMode, self).__init__(game_data)
        self.menu_border = pygame.image.load('img/menuborder.png').convert()
        self.buttonColor = (0, 164, 0)
        self.activeColor = (0, 255, 0)
        self.textColor = (0, 0, 0)
        self.widget_font = pygame.font.Font(regular_text_loc, 28)

        self.widget_zone = pygame.Rect(self.rel_mz_coord((0, 0))[0],
                                       self.rel_mz_coord((0, 0.1))[1],
                                       self.session_data['menu_zone'].width,
                                       self.session_data['menu_zone'].height * 0.9)
        self.widget_row_size = self.widget_zone.height / 10

        # Add menu buttons

        self.fill_buttons()
        if self.menu_mode in sidebars.keys():
            self.widgets = sidebars[self.menu_mode]['widgets']
            self.blit_widgets()
            self.dirty_rects.append(self.widget_zone)

        self.screen.fill(bg_color)

    def rel_gz_coord(self, pos_in_percent):
        """Transforms coordinates in game zone to absolute ones, i.e. [0.5, 0.5] to
        [480, 270] when game zone size is [960, 540]"""
        x = self.session_data['game_zone'][0] + pos_in_percent[0] * self.session_data['game_zone'][2]
        y = self.session_data['game_zone'][1] + pos_in_percent[1] * self.session_data['game_zone'][3]
        return [x, y]

    def rel_mz_coord(self, pos_in_percent):
        """Transforms coordinates in menu zone to absolute ones, i.e. [0.5, 0.5] to
        [480, 270] when game zone size is [960, 540]"""
        x = self.session_data['menu_zone'][0] + pos_in_percent[0] * self.session_data['menu_zone'][2]
        y = self.session_data['menu_zone'][1] + pos_in_percent[1] * self.session_data['menu_zone'][3]
        return [x, y]

    def fill_buttons(self):
        menubuttonsize = [40, 40]
        smallbutton_rect = pygame.Rect(self.rel_mz_coord([0.01, 0.01]),
                                       menubuttonsize)
        if self.menu_mode in sidebars.keys():
            button_list = sidebars[self.menu_mode]['buttons']
        else:
            button_list = ['quit', 'pause', 'options']

        for button in button_list:
            if button == 'save':
                self.add_button(smallbutton_rect, 'S', self.button_font, self.buttonColor,
                                self.activeColor, self.textColor, 'Save')
            elif button == 'pause':
                self.add_button(smallbutton_rect, 'P', self.button_font, self.buttonColor,
                                self.activeColor, self.textColor, 'Pause')
            elif button == 'options':
                self.add_button(smallbutton_rect, 'O', self.button_font, self.buttonColor,
                                self.activeColor, self.textColor, 'Options')
            elif button == 'quit':
                self.add_button(smallbutton_rect, 'E', self.button_font, self.buttonColor,
                                self.activeColor, self.textColor, 'Quit')
            smallbutton_rect.left += menubuttonsize[0] + 2

    def blit_widgets(self):

        widget_position = 0
        pygame.draw.rect(self.screen, self.bg_color, self.widget_zone)
        pygame.draw.rect(self.screen, black, (self.widget_zone.left,
                                              self.widget_zone.top - 5,
                                              self.widget_zone.width,
                                              5))
        for widget in self.widgets:
            widget_rect = pygame.Rect(self.widget_zone.left,
                                      self.widget_zone.top + (self.widget_row_size * widget_position),
                                      self.widget_zone.width,
                                      self.widget_row_size)
            if widget['type'] == 'number':
                message = widget['alias'] + ': ' + str(self.player[widget['stat']])
                self.blit_text(widget_rect.center, message, self.widget_font, black)
                widget_position += 1

            elif widget['type'] == 'bar':
                message = widget['alias']
                self.blit_text(widget_rect.center, message, self.widget_font, black)
                widget_position += 1
                widget_rect.top += self.widget_row_size
                line_rect = widget_rect.inflate(-10, -15)
                pygame.draw.rect(self.screen, bar_bg_color, line_rect)

                if self.player[widget['currstat']] < 0:
                    percent = 0
                else:
                    percent = float(self.player[widget['currstat']]) / self.player[widget['maxstat']]

                line_color = (int(255.0 * (1 - percent)), int(255.0 * percent), 0)
                line_rect.width = line_rect.width * percent
                pygame.draw.rect(self.screen, line_color, line_rect)
                message = str(self.player[widget['currstat']]) + ' / ' + str(self.player[widget['maxstat']])
                self.blit_text(widget_rect.center, message, self.widget_font, black)
                widget_position += 1
            elif widget['type'] == 'image':
                widget_rect.height = widget_rect.height * 3
                if 'img_file' not in widget.keys():
                    filelist = glob.glob(self.sidebar_image_path + '*' + widget['img_name'] + '*')
                    if len(filelist) > 0:
                        if len(filelist) == 1:
                            i = 0
                        else:
                            i = int(random.random() * (len(filelist)))
                        img_path = filelist[i]

                    mini_img = pygame.image.load(img_path)
                    widget['img_file'] = self.aspect_scale(mini_img, (widget_rect.width, widget_rect.height))
                    widget['img_rect'] = widget['img_file'].get_rect()
                    widget['img_rect'].center = widget_rect.center

                self.screen.blit(widget['img_file'], widget['img_rect'])
                widget_position += 3

            pygame.draw.rect(self.screen, black, (widget_rect.left,
                                                  widget_rect.bottom - 5,
                                                  widget_rect.width,
                                                  5))

    def apply_changes(self, changes={}):
        super(GameMode, self).apply_changes(changes)
        self.blit_widgets()
        self.dirty_rects.append(self.widget_zone)

    def display_menu_border(self):
        self.screen.blit(self.menu_border, (self.session_data['game_zone'][2], 0))
        separator = (
            self.session_data['menu_zone'].left,
            self.session_data['menu_zone'].top + self.session_data['menu_zone'].height / 10,
            self.session_data['menu_zone'].width, 5)
        pygame.draw.rect(self.screen, black, separator)

    def process_events(self):
        super_event = super(GameMode, self).process_events()
        if super_event:
            return super_event
        for event in self.events:
            if event.type == MENUEVENT:
                if event.data == 'Quit':
                    return ['mmenu']
            elif event.type == GAMEEVENT:
                return event.data
            elif event.type == CHOICEEVENT:
                if type(event.data[1]) is dict:
                    self.apply_changes(event.data[1])
                return event.data

    def run_frame(self):
        self.display_menu_border()
        self.blit_widgets()

        return super(GameMode, self).run_frame()
