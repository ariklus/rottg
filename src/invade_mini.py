import random
import pygame

from src.config.color_config import bg_color
from src.config.invade_mini_config import unit_stats, sprite_width, sprite_height, shot_size, octo_spacing, fade_time
from src.minigame import MiniGame
from src.utils import Unit


class InvaderMini(MiniGame):
    screen_name = 'invaders'
    menu_mode = 'battle_minigame'
    # Units
    octopi = []
    king_pos = None
    octopi_rect = None
    o_shot = None
    girl = None
    g_shot = None

    def __init__(self, game_data, game_config):
        super(InvaderMini, self).__init__(game_data, game_config)
        if 'difficulcy' in game_config.keys():
            if game_config['difficulcy'] < len(unit_stats):
                difficulcy = game_config['difficulcy']
            elif game_config['difficulcy'] < 0:
                difficulcy = 0
            else:
                difficulcy = len(unit_stats) - 1
        else:
            difficulcy = 3
        self.stats = unit_stats[difficulcy]
        self.gzone = self.session_data['game_zone']
        self.tentacle_img = pygame.image.load(
            'img/tentacle_sprite.png').convert_alpha()
        self.reload_img = pygame.image.load(
            'img/tentacle_sprite_confused.png').convert_alpha()
        self.dead_img = pygame.image.load(
            'img/tentacle_sprite_dead.png').convert_alpha()
        self.king_img = pygame.image.load(
            'img/tentacle_sprite_king.png').convert_alpha()
        self.mgirl_img = pygame.image.load(
            'img/mgirl_sprite1.png').convert_alpha()
        self.g_shot_img = pygame.image.load(
            'img/mgirl_shot.png').convert_alpha()
        self.o_shot_img = pygame.image.load(
            'img/tentacle_shot.png').convert_alpha()
        self.position_units()

    def hitsborder(self, smallrect, zone='gz'):
        if zone == 'gz':
            return not self.gzone.contains(smallrect)

    def move_king(self, direction):
        if self.octopi[self.king_pos].dead:
            return False
        i = 0
        if direction == 'left':
            if self.king_pos > 0:
                for unit in self.octopi[self.king_pos - 1::-1]:
                    i += 1
                    if not unit.dead:
                        self.king_pos -= i
                        return True
        elif direction == 'right':
            if self.king_pos < len(self.octopi) - 1:
                for unit in self.octopi[self.king_pos + 1:]:
                    i += 1
                    if not unit.dead:
                        self.king_pos += i
                        return True
        elif direction == 'up':
            if self.king_pos >= self.stats['row_len']:
                for unit in self.octopi[self.king_pos - self.stats['row_len']::-self.stats['row_len']]:
                    i += 1
                    if not unit.dead:
                        self.king_pos -= i * self.stats['row_len']
                        return True
        elif direction == 'down':
            if self.king_pos < len(self.octopi) - self.stats['row_len']:
                for unit in self.octopi[self.king_pos + self.stats['row_len']::self.stats['row_len']]:
                    i += 1
                    if not unit.dead:
                        self.king_pos += i * self.stats['row_len']
                        return True
        return False

    def print_octopi(self):
        asc_string = ''
        for i in range(0, len(self.octopi)):
            if i % self.stats['row_len'] == 0:
                asc_string += '\n'
            if i == self.king_pos:
                asc_string += 'K'
            elif self.octopi[i].dead is True:
                asc_string += 'X'
            else:
                asc_string += 'O'
        print(asc_string)

    def shoot(self):
        if self.stats['octo_reload'][0] == self.stats['octo_reload'][1]:
            rect = self.octopi[self.king_pos].rect
            self.o_shot = Unit(pos=rect.center,
                               size=shot_size)
            self.stats['octo_reload'][0] = 0

    def update_octorect(self):
        self.dirty_rects.append(self.octopi_rect)
        rects = []
        for unit in self.octopi:
            if not unit.gone:
                rects.append(unit.rect)
        smallrect = rects[0].unionall(rects)
        self.octopi_rect = smallrect.inflate(
            2 * abs(self.stats['octo_speed']), 0)
        self.dirty_rects.append(self.octopi_rect)

    def process_events(self):
        tmp = super(InvaderMini, self).process_events()
        if tmp:
            return tmp
        for event in self.events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.move_king('left')
                elif event.key == pygame.K_RIGHT:
                    self.move_king('right')
                elif event.key == pygame.K_UP:
                    self.move_king('up')
                elif event.key == pygame.K_DOWN:
                    self.move_king('down')
                elif event.key == pygame.K_SPACE:
                    self.shoot()

    def position_units(self):
        self.screen.fill(bg_color)
        # Position fleet of octopi
        self.octopi = []

        oy_start = self.rel_gz_coord([0, 0.03])[1]
        ox_start = random.randint(self.stats['octo_speed'],
                                  self.gzone[2] - (sprite_width + octo_spacing) * self.stats['row_len'])
        for i in range(0, self.stats['unit_number']):
            if i % self.stats['row_len'] == 0:
                oy_start += sprite_height + octo_spacing
                i = 0
            octo = Unit(pos=[ox_start + (i % self.stats['row_len']) * (sprite_width + octo_spacing), oy_start],
                        size=[sprite_width, sprite_height])
            self.octopi.append(octo)

        self.king_pos = random.randint(0, len(self.octopi) - 1)

        self.update_octorect()
        # Position enemy magical girl
        gy_start = self.gzone[3] - sprite_height
        gx_start = random.randint(
            self.stats['girl_speed'], self.gzone[2] - sprite_width)
        self.girl = Unit(pos=[gx_start, gy_start],
                         size=[sprite_width, sprite_height])
        self.girl.counter = self.stats['girl_reload'] + random.randint(0, 30)

    def run_frame(self, menu_mode='invaders'):
        self.screen.fill(bg_color)
        # Check direction change condition
        if self.hitsborder(smallrect=self.octopi_rect):
            self.stats['octo_speed'] *= -1
            for unit in self.octopi:
                unit.move([0, sprite_width])
        # move&draw octopi
        u = 0
        for unit in self.octopi:
            if not unit.gone:
                unit.move([self.stats['octo_speed'], 0])
                if self.g_shot is not None:
                    if unit.rect.colliderect(self.g_shot):
                        unit.dead = True
                        unit.counter = fade_time
                        self.dirty_rects.append(self.g_shot.rect)
                        self.g_shot = None
                if unit.dead is True:
                    self.screen.blit(self.dead_img, unit.rect)
                    unit.counter -= 1
                    if unit.counter <= 0:
                        if u == self.king_pos:
                            self.lose()
                        unit.gone = True
                        pygame.draw.rect(self.screen, bg_color, unit.rect)
                elif u == self.king_pos:
                    if self.stats['octo_reload'][0] < self.stats['octo_reload'][1]:
                        self.stats['octo_reload'][0] += 1
                        self.screen.blit(self.reload_img, unit.rect)
                    else:
                        self.screen.blit(self.king_img, unit.rect)
                else:
                    self.screen.blit(self.tentacle_img, unit.rect)
            u += 1

        self.update_octorect()
        # Control magical girl
        if self.hitsborder(smallrect=self.girl.rect.inflate(abs(self.stats['girl_speed'] * 2), 0)):
            self.stats['girl_speed'] *= -1
        else:
            if random.randint(0, 100) == 0:
                self.stats['girl_speed'] *= -1
        if self.g_shot is None:
            if self.girl.counter == 0:
                self.g_shot = Unit(pos=self.girl.rect.center,
                                   size=shot_size)
                self.girl.counter = self.stats['girl_reload'] + random.randint(0, 10)
            else:
                self.girl.counter -= 1
        # move&draw magical girl
        self.dirty_rects.append(self.girl.rect)
        self.girl.move([self.stats['girl_speed'], 0])
        self.screen.blit(self.mgirl_img, self.girl.rect)
        self.dirty_rects.append(self.girl.rect)
        # Move projectiles
        if self.g_shot is not None:
            self.dirty_rects.append(self.g_shot.rect)
            if self.hitsborder(smallrect=self.g_shot.rect):
                self.g_shot = None
            else:
                self.g_shot.move([0, -1 * self.stats['girlshot_speed']])
                self.screen.blit(self.g_shot_img, self.g_shot.rect)
                self.dirty_rects.append(self.g_shot.rect)

        if self.o_shot is not None:
            self.dirty_rects.append(self.o_shot.rect)
            if self.hitsborder(smallrect=self.o_shot.rect):
                self.o_shot = None
            else:
                self.o_shot.move([0, self.stats['octoshot_speed']])
                self.screen.blit(self.o_shot_img, self.o_shot.rect)
                self.dirty_rects.append(self.o_shot.rect)
        # Check win conditions
        if self.octopi_rect[1] + self.octopi_rect[3] > self.gzone[3] - sprite_height:
            self.win()
        if self.o_shot is not None:
            if self.o_shot.rect.colliderect(self.girl.rect):
                self.win()

        return super(InvaderMini, self).run_frame()
