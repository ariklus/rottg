import copy

import pygame

from src.config.events_config import MENUEVENT, CHOICEEVENT, GAMEEVENT, TRANSFEREVENT, RADIOEVENT


class Button(object):
    """ This class keeps button position, text, colors and action"""
    event_type = MENUEVENT

    def __init__(self, rect, text, font, inactive_color, active_color, text_color, action):
        self.rect = pygame.Rect(rect)
        self.text = text
        self.inactive_color = inactive_color
        self.active_color = active_color
        self.action = action
        self.font = font
        self.text_color = text_color
        self.active = -1
        self.pressed = False
        self.event = pygame.event.Event(self.event_type, data=self.action)

    def color(self):
        if self.active:
            return self.active_color
        else:
            return self.inactive_color

class ChoiceButton(Button):
    event_type = CHOICEEVENT

    def __init__(self, rect, text, font, inactive_color, active_color, text_color, choice_effects):
        self.effects = choice_effects['content']
        super(ChoiceButton, self).__init__(rect, text, font, inactive_color,
                                           active_color, text_color, choice_effects["screen"])
        self.event = pygame.event.Event(self.event_type, data=[choice_effects["screen"], self.effects])


class TransferButton(Button):
    event_type = TRANSFEREVENT


class SwitchButton(object):
    event_type = RADIOEVENT

    def __init__(self, rect, lefttext, righttext, font, text_color, leftaction, rightaction,
                 inactive_color, active_color, active, header=None):
        self.rect = pygame.Rect(rect)
        self.left_rect = pygame.Rect(rect)
        self.left_rect.width -= self.rect.width/2
        self.right_rect = copy.deepcopy(self.left_rect)
        self.right_rect.left += self.left_rect.width

        self.left_text = lefttext
        self.right_text = righttext
        self.inactive_color = inactive_color
        self.active_color = active_color
        self.leftaction = leftaction
        self.rightaction = rightaction
        self.font = font
        self.text_color = text_color
        self.active = active
        self.pressed = False
        self.leftevent = pygame.event.Event(self.event_type, data=self.leftaction)
        self.rightevent = pygame.event.Event(self.event_type, data=self.rightaction)
        self.header = header

    def clickable_rect(self):
        if self.active:
            return self.left_rect
        else:
            return self.right_rect

    def switch(self):
        if self.active is True:
            self.active = False
            return self.leftevent
        else:
            self.active = True
            return self.rightevent

    def left_color(self):
        if self.active:
            return self.inactive_color
        else:
            return self.active_color

    def right_color(self):
        if not self.active:
            return self.inactive_color
        else:
            return self.active_color


class Unit:
    counter = 0
    gone = False
    dead = False

    def __init__(self, pos=None, size=(50, 50)):
        if pos is not None:
            self.rect = pygame.Rect(pos[0], pos[1], size[0], size[1])

    def move(self, update):
        self.rect = self.rect.move(update)
