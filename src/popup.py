import pygame
from game_object import GameObject


class SmallPopup(GameObject):
    def __init__(self, game_data, popup_type):
        super(SmallPopup, self).__init__(game_data)
        popup_size = [300, 150]
        self.popup_rect = [(game_data['size'][0]-popup_size[0])/2,
                           (game_data['size'][1] - popup_size[1])/2,
                           popup_size]
        self.dirty_rects = [self.popup_rect]

    def process_events(self):
        super(SmallPopup, self).__init__(game_data)

    def run_frame(self):
        while True:
            self.process_events()
            self.dirty_rects = [self.popup_rect]
