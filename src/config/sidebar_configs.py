sidebars = {
    'battle_minigame': {'buttons': ['quit', 'pause'],
                        'widgets': [
                            {'type': 'number', 'stat': 'strength', 'alias': 'Army'},
                            {'type': 'bar', 'currstat': 'hp', 'maxstat': 'strength', 'alias': 'HP'}
                            ]
                        },
    'map': {
        'buttons': ['save', 'options', 'quit'],
        'widgets': [{'type': 'number', 'stat': 'strength', 'alias': 'Army_size'},
                    {'type': 'number', 'stat': 'avatars', 'alias': 'AVATARS'},
                    {'type': 'bar', 'currstat': 'monsters', 'maxstat': 'lair', 'alias': 'Lair capacity'}]
    },
    'story': {'buttons': ['save', 'options', 'quit'],
              'widgets': [{'type': 'number', 'stat': 'strength', 'alias': 'Army size'},
                          {'type': 'image', 'img_name': 'cute'}]}
}
