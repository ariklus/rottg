import pygame

default_fps = 30
# screen resolution (fuck resizing)

size = (960, 540)
# splitting menu/info and game area
game_zone = pygame.Rect(0, 0, 755, 540)
menu_zone = pygame.Rect(760, 0, 200, 540)
game_name = 'Rise Of The Tentacle God'

game_data = {'size': size,
            'curr_size': size,
            'game_zone': game_zone,
            'menu_zone': menu_zone,
            'game_name': game_name,
            'default_fps': default_fps,
            }