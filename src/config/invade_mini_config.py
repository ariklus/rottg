# tech details
sprite_width, sprite_height = 50, 50
shot_size = [10, 10]
octo_spacing = 20
fade_time = 30
# Unit stats (depending on difficulcy)
unit_stats = [
    {'unit_number': 18,
     'row_len': 6,
     'octo_speed': 2,
     'octo_reload': [30, 30],
     'octoshot_speed': 20,
     'girl_speed': 5,
     'girl_reload': 60,
     'girlshot_speed': 10},

    {'unit_number': 18,
     'row_len': 6,
     'octo_speed': 2,
     'octo_reload': [45, 45],
     'octoshot_speed': 20,
     'girl_speed': 5,
     'girl_reload': 45,
     'girlshot_speed': 15},

    {'unit_number': 12,
     'row_len': 6,
     'octo_speed': 1,
     'octo_reload': [60, 60],
     'octoshot_speed': 15,
     'girl_speed': 6,
     'girl_reload': 30,
     'girlshot_speed': 15},

    {'unit_number': 12,
     'row_len': 6,
     'octo_speed': 1,
     'octo_reload': [75, 75],
     'octoshot_speed': 10,
     'girl_speed': 8,
     'girl_reload': 15,
     'girlshot_speed': 15},

    {'unit_number': 6,
     'row_len': 6,
     'octo_speed': 1,
     'octo_reload': [90, 90],
     'octoshot_speed': 10,
     'girl_speed': 10,
     'girl_reload': 0,
     'girlshot_speed': 20}
]
