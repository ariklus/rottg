# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 21:55:43 2017

@author: ariklus
"""
# import sys
import pygame

from game_object import GameObject
from src.config.color_config import *
from src.config.events_config import MENUEVENT
from src.config.font_config import title_font_loc


class MainMenu(GameObject):
    screen_name = 'mmenu'
    menubutton_size = [200, 50]

    def __init__(self, gamedata):
        super(MainMenu, self).__init__(gamedata)
        self.bg_img = self.aspect_scale(pygame.image.load("img/menu_background.png").convert(),
                                        (self.screen.get_width(), self.screen.get_height()))
        print(self.bg_img)
        self.startmenu_x = self.screen.get_width() * 0.5 - 100
        self.startmenu_y = self.session_data['size'][1] * 0.5 - 60
        self.title_font = pygame.font.Font(title_font_loc, 50)
        self.title_center = self.screen.get_width()*0.5, self.screen.get_height()*0.2
        self.menu_button_rect = [self.startmenu_x,
                                 self.startmenu_y,
                                 self.menubutton_size[0],
                                 self.menubutton_size[1]]

        self.add_transfer_button(self.menu_button_rect, 'New Game', self.button_font,
                                 inactive_button_color, active_button_color, black,
                                 {'screen': 'load', 'content': 'new'})
        self.menu_button_rect[1] += 60
        self.add_transfer_button(self.menu_button_rect, 'Continue', self.button_font,
                                 inactive_button_color, active_button_color, black,
                                 {'screen': 'load', 'content': 'load'})
        self.menu_button_rect[1] += 60
        self.add_button(self.menu_button_rect, 'Options', self.button_font,
                        inactive_button_color, active_button_color, black, 'Options')
        self.menu_button_rect[1] += 60
        self.add_button(self.menu_button_rect, 'Credits', self.button_font,
                        inactive_button_color, active_button_color, black, 'Credits')
        self.menu_button_rect[1] += 60
        self.add_button(self.menu_button_rect, 'Exit', self.button_font,
                        inactive_button_color, active_button_color, black, 'Exit')

    def process_events(self):
        tmp = super(MainMenu, self).process_events()
        if tmp:
            return tmp

        for event in self.events:
            if event.type == MENUEVENT:
                if event.data == 'New Game':
                    return self.new_game(2)
                if event.data == 'Exit':
                    return ['exit']

    def run_frame(self):
        # Background
        self.screen.blit(self.bg_img, (0, 0))
        # Game Title
        self.blit_text(position=self.title_center,
                       text=self.session_data['game_name'],
                       font=self.title_font,
                       color=black,
                       bgcolor=white,
                       alpha=50)

        return super(MainMenu, self).run_frame()
