import copy
import glob
import json

import pygame
from game_mode import GameMode
from config.events_config import TRANSFEREVENT


# noinspection PyUnresolvedReferences
class MapMode(GameMode):
    screen_name = 'map'
    menu_mode = 'map'
    displayed_name = None

    grid_size = 1
    grid_rect = None
    # Map's backround_images
    background = None
    bg_rect = None

    grid = []
    grid_object = {'image': {'inactive': None,
                             'active': None,
                             'selected': None},
                   'position': 1,
                   'active': False,
                   'clickable': False,
                   'on_click': None,
                   'condition': None,
                   'rect': None,
                   'selected': False,
                   'pressed': False
                   }

    def __init__(self, game_data, map_name):
        super(MapMode, self).__init__(game_data)
        self.folder_path = "maps/" + map_name + '/'

        self.load_background()
        self.load_grid()

        self.blit_background()
        self.blit_grid_objects()
        self.dirty_rects.append(self.session_data['game_zone'])

    def run_frame(self):
        self.blit_background()
        self.blit_grid_objects()
        return super(MapMode, self).run_frame()

    def load_background(self):
        self.screen.fill(self.bg_color)
        path = glob.glob(self.folder_path + 'background' + '*')
        if len(path) > 0:
            self.background = pygame.image.load(path[0])
            self.background = self.aspect_scale(self.background,
                                                (self.session_data['game_zone'][2],
                                                 self.session_data['game_zone'][3]))
            self.bg_rect = self.background.get_rect()
            self.bg_rect.center = self.session_data['game_zone'].center
            self.screen.blit(self.background, self.bg_rect)

    def blit_background(self):
        self.screen.fill(self.bg_color)
        if self.background is not None:
            self.screen.blit(self.background, self.bg_rect)

    def load_grid(self):
        json_data = open(self.folder_path + 'stats.json').read()
        grid_config = json.loads(json_data)

        self.displayed_name = grid_config['displayed_name']
        self.grid_size = grid_config['grid_size']
        self.grid_rect = pygame.Rect((0, 0),
                                     (self.session_data['game_zone'][2]/self.grid_size,
                                      self.session_data['game_zone'][3] / self.grid_size))

        for item in grid_config['grid_objects']:
            self.grid.append(self.make_grid_object(item))

    def make_grid_object(self, stats):
        grid_object = copy.deepcopy(self.grid_object)

        grid_object['clickable'] = stats['clickable']

        grid_object['on_click'] = stats['on_click']

        if stats['position'] > (self.grid_size**2):
            raise ValueError("Map object position out of grid")
        else:
            grid_object['position'] = stats['position']

        x = stats['position'] % self.grid_size
        y = stats['position'] / self.grid_size

        grid_object['rect'] = pygame.Rect(self.grid_rect)
        grid_object['rect'].topleft = (self.grid_rect.width * x, self.grid_rect.height * y)

        if stats['condition'] is None:
            grid_object['active'] = True
        else:
            grid_object['active'] = self.check_condition(stats['condition']['type'],
                                                         stats['condition']['name'],
                                                         stats['condition']['comparation'])

        for state in ['active', 'inactive', 'selected']:
            search = self.folder_path + str(grid_object['position']) + "_" + state + '*'
            path = glob.glob(search)

            if len(path) > 0:
                image = pygame.image.load(path[0])
                grid_object['image'][state] = self.aspect_scale(image,
                                                                (grid_object['rect'].width,
                                                                 grid_object['rect'].height))

        return grid_object

    def blit_grid_objects(self):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()

        for grid_object in self.grid:
            if grid_object['active']:
                if grid_object['rect'].collidepoint(mouse):
                    if grid_object['image']['selected'] is not None:
                        self.screen.blit(grid_object['image']['selected'], grid_object['rect'])
                        if not grid_object['selected']:
                            self.dirty_rects.append(grid_object['rect'])
                            grid_object['selected'] = True
                    if click[0] == 1 and grid_object['clickable']:
                        grid_object['pressed'] = True
                    elif click[0] == 0 and grid_object['pressed'] and grid_object['clickable']:
                        event = pygame.event.Event(TRANSFEREVENT, data=grid_object['on_click'])
                        pygame.event.post(event)
                elif grid_object['image']['active'] is not None:
                    grid_object['pressed'] = False
                    self.screen.blit(grid_object['image']['active'], grid_object['rect'])
                    if grid_object['selected']:
                        self.dirty_rects.append(grid_object['rect'])
                        grid_object['selected'] = False
                else:
                    grid_object['pressed'] = False

            else:
                if grid_object['image']['inactive'] is not None:
                    self.screen.blit(grid_object['image']['inactive'], grid_object['rect'])
