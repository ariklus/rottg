import copy
import glob
import json
import sys

from src.config.screen_lists import minigames_list, oneword_list


# noinspection PyUnresolvedReferences,PyTypeChecker
class ScriptMaker(object):

    standard_line = {
        "type": '',
        "content": None
    }
    newlines = ['M', 'I', 'R', 'C']
    addline = ['S', 'F', 'W', 'L', 'G']

    def __init__(self, textfile_name, scriptfile_name):
        self.text_file = open(textfile_name, 'r')
        self.outputfile_name = scriptfile_name
        self.script_body = []
        self.current_script_line = copy.deepcopy(self.standard_line)
        self.collection = {}
        self.collecting = False
        self.lines = []
        self.line = []
        self.position = 0
        self.cut_prefix = 0

    def make_script(self):

        for line in self.text_file:
            if line[0].isalpha():
                self.lines.append(line.replace('\n', ''))
        print self.position
        self.line = self.lines[self.position]

        while self.position < len(self.lines):
            self.process_line()
            self.next_line()
        self.push_line()

        print self.script_body
        f = open(self.outputfile_name, 'a')
        f.seek(0)
        f.truncate()
        dump = json.dumps(self.script_body, indent=4)
        f.write(dump)

    def next_line(self):
        self.position += 1
        if self.position < len(self.lines):
            self.line = self.lines[self.position]

    def process_line(self, text=None):
        if text is None:
            if self.cut_prefix > 0:
                line = self.lines[self.position][self.cut_prefix:]
            else:
                line = self.lines[self.position]
        else:
            line = text

        # Processing common text
        if line[0] == 'T':
            if self.current_script_line['type'] == 'text' and len(self.current_script_line['content']) < 5:
                self.current_script_line['content'].append(line[2:])
            else:
                self.push_line()
                self.current_script_line['type'] = 'text'
                self.current_script_line['content'] = [line[2:]]

        elif line[0] == 'B':
            self.push_line()

        elif line[0] in self.newlines:
            self.push_line()

            # Transfer to other screens. Includes reading data from
            if line[0] == 'M':
                self.current_script_line['content'] = {}
                position = line.find(':')
                screen = line[2:position]
                self.current_script_line['type'] = 'transfer'
                self.current_script_line['screen'] = screen
                if screen in oneword_list:
                    self.current_script_line['content'] = line[position + 2:]
                elif screen in minigames_list:
                    if len(line[position + 2:]) > 0:
                        self.current_script_line['content']['difficulcy'] = int(line[position + 2:])
                    self.collecting = True
                    self.next_line()
                    self.collect_minigame_data()
                    return True
                elif screen == 'choice':
                    self.current_script_line['content']['text'] = line[position + 2:]
                    self.collecting = True
                    self.next_line()
                    self.collect_choice_data()
                    return True

            # Processing images
            elif line[0] == 'I':

                self.current_script_line['type'] = 'image'
                self.current_script_line['content'] = line[2:]

            # Binary outcomes
            elif line[0] in ['R', 'C']:
                position = line.find(':')
                info = [line[3:position], line[position + 2:]]

                if line[0] == 'R':
                    self.current_script_line['type'] = 'random'
                    self.current_script_line['content'] = {'yeslines': [],
                                                           'nolines': []}
                    self.current_script_line['chance'] = int(line[2:])

                elif line[0] == 'C':
                    self.current_script_line['type'] = 'condition'
                    self.current_script_line['content'] = {'condition': {'name': info[0],
                                                                         'comparation': info[1]},
                                                           'yeslines': [],
                                                           'nolines': []}
                    if line[1] == 'F':
                        self.current_script_line['content']['condition']['type'] = 'flag'
                    elif line[1] == 'S':
                        self.current_script_line['content']['condition']['type'] = 'stat'

                self.collecting = True
                self.next_line()
                self.collect_binary_outcome_data()

        # Lines appended to existing script line
        elif line[0] in self.addline:

            # Editing stats and/or flags
            if line[0] == 'S':
                if 'stat_change' not in self.current_script_line.keys():
                    self.current_script_line['stat_change'] = {}
                position = line.find(':')
                stat = line[2:position]
                self.current_script_line['stat_change'][stat] = int(line[position + 2:].replace('\n', ''))

            elif line[0] == 'F':
                if 'flag_change' not in self.current_script_line.keys():
                    self.current_script_line['flag_change'] = {}
                position = line.find(':')
                flag = line[2:position]
                self.current_script_line['flag_change'][flag] = bool(line[position + 2:].replace('\n', ''))
            elif line[0] == 'G':
                if 'widgets' not in self.current_script_line.keys():
                    self.current_script_line['widgets'] = []
                position = line.find(':')
                action = line[2:position]
                if action == 'add':
                    widget = eval(line[position + 2:].replace('\n', ''))
                else:
                    widget = None
                widgetline = {'action': action, 'widget': widget}
                self.current_script_line['widgets'].append(widgetline)


    def collect_binary_outcome_data(self):
        self.collecting = True
        binary = copy.copy(self.current_script_line)
        self.current_script_line = copy.deepcopy(self.standard_line)
        self.cut_prefix += 2
        line_start = self.cut_prefix-2

        while self.position < len(self.lines) and self.line[line_start] in ['Y', 'N']:
            if self.line[line_start] == 'Y':
                destination = 'yeslines'
                currtype = 'Y'
            else:
                destination = 'nolines'
                currtype = 'N'

            while self.line[line_start] == currtype and self.position < len(self.lines):
                self.process_line()
                self.next_line()

            binary['content'][destination].append(self.current_script_line)
            self.current_script_line = copy.deepcopy(self.standard_line)

        self.push_collection(binary)
        self.cut_prefix -= 2
        self.position -= 1

    def collect_minigame_data(self):
        self.collecting = True
        game = copy.copy(self.current_script_line)
        self.cut_prefix += 2
        line_start = self.cut_prefix-2

        while self.position < len(self.lines) and self.line[line_start] in ['W', 'L']:
            if self.line[line_start] == 'W':
                destination = 'on_win'
                currtype = 'W'
            else:
                destination = 'on_loss'
                currtype = 'L'

            while self.line[line_start] == currtype and self.position < len(self.lines):
                self.process_line()
                self.next_line()

                game['content'][destination] = self.current_script_line
            self.current_script_line = copy.deepcopy(self.standard_line)

        self.push_collection(game)
        self.cut_prefix -= 2
        self.position -= 1

    def collect_choice_data(self):
        self.collecting = True
        choice = copy.copy(self.current_script_line)
        choice['content']['buttons'] = []
        self.cut_prefix += 3

        while self.position < len(self.lines) and self.line[0] == 'C':
            button_text = self.line[3:].replace('\n', '')
            self.next_line()

            button_num = self.line[1]

            while self.line[1] == button_num and self.position < len(self.lines):
                self.process_line()
                self.next_line()

            choice['content']['buttons'].append(self.current_script_line)
            choice['content']['buttons'][int(button_num) - 1]['text'] = button_text
            self.current_script_line = copy.deepcopy(self.standard_line)

        self.push_collection(choice)
        self.position -= 1
        self.cut_prefix -= 3

    def push_line(self):
        if self.current_script_line['type'] != '' and self.collecting is False:
            self.script_body.append(self.current_script_line)
            self.current_script_line = copy.deepcopy(self.standard_line)

    def push_collection(self, collection):
        self.current_script_line = collection
        self.collecting = False

print sys.path
if len(sys.argv) == 1:
    filenames = glob.glob('*.script')
else:
    filenames = [sys.argv[1]]

print filenames

for scriptfile in filenames:
    outfile = scriptfile.replace(".script", ".json")
    #outfile = scriptfile.replace("script_maker", "../scripts/")
    outfile = '../../scripts/' + outfile
    maker = ScriptMaker(scriptfile, outfile)
    maker.make_script()
