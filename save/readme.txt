It's really easy to cheat by editing save files.
But game is easy enough as it is, so do it only if you lost our previous saves.
Also, fiddling with flags manually may result in making you stuck.