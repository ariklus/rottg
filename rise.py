#!/usr/bin/env python

from src.config.general_config import game_data
from src.runner import Runner
import os

os.chdir(os.getcwd())
game = Runner(game_data)
game.run_game()
